require 'spec_helper'

describe LogService::File do
  let(:service) { LogService::File.new(file) }
  let(:file) { StringIO.new('test') }
  let(:strategy) { double('strategy') }

  it 'should properly initialized' do
    expect(service).to be
  end

  describe 'strategy evaluation' do
    before do
      allow(strategy).to receive(:kind_of?)
        .with(LogService::Strategy::Base)
        .and_return(true)
    end

    context 'strategy is not strategy' do
      before do
        allow(strategy).to receive(:kind_of?)
          .with(LogService::Strategy::Base)
          .and_return(false)
      end

      it 'should raise StrategyIsNotValidError' do
        expect{ service.run(strategy) }
          .to raise_error(LogService::StrategyIsNotValidError)
      end
    end

    context 'when file is present and valid' do
      it 'should run strategy with file' do
        expect(strategy).to receive(:execute)
          .with(file)
          .and_return(true)

        service.run(strategy)
      end
    end

    context 'when file is not present' do
      let(:file) { nil }

      it 'should raise FileIsNotValidError' do
        expect{ service.run(strategy) }
          .to raise_error(LogService::FileIsNotValidError)
      end
    end
  end
end
