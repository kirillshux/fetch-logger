require 'spec_helper'
require 'date'

text = <<-TEXT
I, [2017-11-06T08:57:03.004635 #14461]  INFO -- : [42828b9e-2eb8-4350-aedf-87c529e1f36d] Processing by V1::CartController#info as */*
I, [2017-11-06T19:15:23.890654 #14461]  INFO -- : [625be618-e15f-4f86-ad80-0e14eda47fa9] Completed 200 OK in 12ms✓✓✓ (Views: 9.1ms | ActiveRecord: 0.6ms)
Multiline message
I, [2017-11-07T19:15:23.890654 #14461]  INFO -- : [625be618-e15f-4f86-ad80-0e14eda47fa9] Completed 200 OK in 12ms (Views: 9.1ms | ActiveRecord: 0.6ms)
I, [2017-11-08T08:57:02.991258 #14461]  INFO -- : [42828b9e-2eb8-4350-aedf-87c529e1f36d] Started GET "/api/v1/cart✓✓✓" for 46.8.117.80 at 2017-11-08 08:57:02 +0300
I, [2017-11-08T08:57:02.998209 #14461]  INFO -- : [f5be022f-c0e5-48cb-9fba-cde650449959] Started GET "/api/v1/stores/29/categories" for 46.8.117.80 at 2017-11-08 08:57:02 +0300
Multiline message
Multiline message✓✓✓
Multiline message✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓
I, [2017-11-09T08:57:03.004635 #14461]  INFO -- : [42828b9e-2eb8-4350-aedf-87c529e1f36d] Processing by V1::CartController#info as */*
I, [2017-11-09T08:57:03.006262 #14461]  INFO -- : [723bc6e3-b13c-4a56-878b-00dc309ecf33] Started GET "/api/v1/users/info" for 46.8.117.80 at 2017-11-08 08:57:03 +0300
TEXT

describe LogService::Strategy::DaySearch do
  let(:strategy) { LogService::Strategy::DaySearch.new(date) }
  let(:file) { StringIO.new(text) }

  describe 'date search from file' do
    context 'when file includes date' do
      let(:date) { Date.new(2017, 11, 7) }

      it 'should return array with proper string' do
        expect(strategy.execute(file)).to eq([
          "I, [2017-11-07T19:15:23.890654 #14461]  INFO -- : [625be618-e15f-4f86-ad80-0e14eda47fa9] Completed 200 OK in 12ms (Views: 9.1ms | ActiveRecord: 0.6ms)"
        ])
      end
    end

    context 'when file not includes date' do
      let(:date) { Date.new(2017, 11, 2) }

      it 'should return empty array' do
        expect(strategy.execute(file)).to eq([])
      end
    end
  end
end
