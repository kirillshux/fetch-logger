lib_root = File.expand_path('../lib', __dir__)
$LOAD_PATH.unshift lib_root

require 'log_service'

require 'ostruct'
require 'optparse'
require 'pry'
require 'colorize'

options = OpenStruct.new

opt_parser = OptionParser.new do |opts|
  opts.banner = "Usage: fetch-logger [--help] [-c <COUNT>]
  [-d <DATE>] [-s <SUBSTRING>] <command> [<args>]"

  opts.separator ''
  opts.separator 'Specific options:'

  opts.on('-f', '--file FILE', 'Set source file to parse') do |file|
    options.file = file
  end

  opts.on('-c', '--count COUNT', 'Parse file at given Count') do |count|
    options.count = count
  end

  opts.on('-s', '--substring SUBSTRING', 'Parse file at given Substring') do |subs|
    options.subs = subs
  end

  opts.on('-d', '--date DATE', 'Parse file at given Date') do |date|
    options.date = date
  end

  opts.separator 'Common options:'

  opts.on_tail('-h', '--help', 'Show this message') do
    puts opts
    exit
  end
end

opt_parser.parse!(ARGV)

unless options.file && File.exist?(options.file)
  puts <<~ERROR
  #{'ERROR'.red}: File #{options.file.cyan} not found. Try to perform an existed file.
  ERROR

  exit 1
end

def create_strategy(name, *args)
  raise "No strategy with name #{name}" unless LogService::Strategy.const_defined?(name)

  strategy_class = LogService::Strategy.const_get(name)
  strategy_class.new(*args)
end

# Fill strategies
strategies = []

strategies << create_strategy('DaySearch', options.date) if options.date
strategies << create_strategy('SubstringSearch', options.subs) if options.subs
strategies << create_strategy('CountSearch', options.count) if options.count

file = File.open(options.file)
log_service = LogService::File.new(file)

result = log_service.run(strategies)

puts result.to_a
