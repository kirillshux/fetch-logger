require 'date'

module LogService
  module Strategy
    class DaySearch < Base
      include LogService::Utils::FileNavigation
      include LogService::Utils::MessageEnumerator

      TIME_REGEXP = /\d{4}\-\d{2}\-\d{2}T\d{2}\:\d{2}\:\d{2}\.\d{6}/

      def initialize(date)
        @date = Date.parse(date)
      end

      def execute(file)
        date_search(file)
      end

      protected

      # Method returns an StringIO with messages with a given date
      def date_search(file)
        min, max = binary_search_and_hunt(file)
        return nil unless max && min
        file.pos = min

        result = StringIO.new
        result.write(file.read(max - min))

        result.rewind
        file.rewind
        result
      end

      # Method returns an Array of the beginning and end positions
      # of messages with a given date
      def binary_search_and_hunt(file)
        found_pos = binary_search(file)
        return nil unless found_pos
        max = get_right_border(file, found_pos)
        min = get_left_border(file, found_pos)
        file.rewind
        [min, max]
      end

      # Algorithm for finding the position of the beginning of messages with
      # the desired date
      def get_right_border(file, pos)
        max = pos
        limit = file.size - 1
        compare = 0
        step = 30_000

        # Looking for message with a date later than the current one
        # with step = 30_000 byte, min - last position with the same date
        until compare == 1 || max >= limit
          min = max
          max += step
          max = limit if max > limit # if we found eof

          file.pos = max
          goto_start_message(file)

          compare = get_line_and_compare_date(file)
        end

        compare = 0
        file.pos = min

        # Linear search for a last position of message with the same date
        until compare == 1 || min >= max
          enum = Utils::MessageEnumerator.from_file(file).lazy
          enum.next
          min = file.pos
          min = max if min >= max
          compare = get_line_and_compare_date(file)
        end
        file.rewind
        min
      end

      def get_left_border(file, pos)
        min = pos
        compare = 0
        step = 30_000

        # Looking for message with a date earlier than the current one
        # with step = 30_000 byte, max - last position with the same date
        until compare == -1 || min.zero?
          max = min
          min -= step
          min = 0 if min.negative?
          file.pos = min
          goto_start_message(file)
          
          compare = get_line_and_compare_date(file)
        end

        file.pos = max
        compare = 0

        # Linear search for a last position of message with the same date
        until compare == -1 || max <= min # + 5 for get some space
          max = file.pos
          goto_prev_line(file)
          goto_start_message(file)
          
          compare = get_line_and_compare_date(file)
        end
        file.rewind
        max
      end

      # Base search algorithm for the position of a message
      # with the desired date (returns the position of only one message)
      def binary_search(file)
        left = 0
        right = file.size - 1

        found_pos = nil
        while left <= right
          bin_step = (left + right) / 2
          file.pos = bin_step

          goto_start_message(file)
          
          case get_line_and_compare_date(file)
          when 0
            found_pos = bin_step
            break
          when 1
            right = bin_step - 1
          when -1
            left = bin_step + 1
          end
        end
        file.rewind
        found_pos
      end

      # Get line and string and compare it with the search date
      # return -1 if date in line earlier
      # return 0 if date if the same
      # return 1 if date is later
      def get_line_and_compare_date(file)
        line = get_line(file)
        date_match = TIME_REGEXP =~ line
        found_date_str = line[date_match..date_match + 9]
        found_date = Date.parse(found_date_str)
        compare = found_date <=> @date
        compare
      end
    end
  end
end
