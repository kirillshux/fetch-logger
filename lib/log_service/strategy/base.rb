# frozen_string_literal: true

module LogService
  module Strategy
    class Base
      def execute(_file)
        raise 'method #execute should be overriden'
      end
    end
  end
end
