# frozen_string_literal: true

module LogService
  module Strategy
    class SubstringSearch < Base
      include Utils::MessageEnumerator

      def initialize(substr)
        @substr = substr
      end

      def execute(file)
        retutn nil unless file
        substring_search(file)
      end

      protected

      def substring_search(file)
        result = StringIO.new
        enum = Utils::MessageEnumerator.from_file(file).lazy

        enum
          .select { |mes| mes.include?(@substr) }
          .each { |mes| result.write(mes) }
        
        result.rewind
        file.rewind
        result
      end
    end
  end
end
