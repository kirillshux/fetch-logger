# frozen_string_literal: true

module LogService
  module Strategy
    class RegexpSearch < Base
      include Utils::MessageEnumerator

      def initialize(regexp)
        @regexp = regexp
      end

      def execute(file)
        retutn nil unless file
        regexp_search(file)
      end

      protected

      def regexp_search(file)
        result = StringIO.new
        enum = Utils::MessageEnumerator.from_file(file).lazy
        enum
          .select { |mes| mes.match?(@regexp) }
          .each { |mes| result.write(mes) }

        file.rewind
        result.rewind
        result
      end
    end
  end
end
