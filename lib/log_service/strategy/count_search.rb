# frozen_string_literal: true

module LogService
  module Strategy
    class CountSearch < Base
      include Utils::MessageEnumerator

      def initialize(count)
        @count = count.to_i
      end

      def execute(file)
        return nil unless file
        count_search(file)
      end

      protected

      def count_search(file)
        result = StringIO.new
        enum = Utils::MessageEnumerator.from_file(file).lazy

        enum
          .take(@count)
          .each { |mes| result.write(mes) }
        
        result.rewind
        file.rewind
        result
      end
    end
  end
end
