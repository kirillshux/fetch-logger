# frozen_string_literal: true

module LogService
  class File
    def initialize(file)
      raise FileIsNotValidError unless file.is_a?(IO) || file.is_a?(StringIO)

      @file = file
    end

    def run(strategies)
      raise StrategyIsNotValidError unless strategies.all? do |strategy|
        strategy.is_a?(Strategy::Base)
      end

      strategies.reduce(@file) do |buffer, strat|
        strat.execute(buffer)
      end
    end
  end
end
