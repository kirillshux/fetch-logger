# frozen_string_literal: true

require 'log_service/strategy/base'
require 'log_service/strategy/day_search'
require 'log_service/strategy/substring_search'
require 'log_service/strategy/regexp_search'
require 'log_service/strategy/count_search'
