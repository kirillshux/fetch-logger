module LogService
  module Utils
    module MessageEnumerator
      module_function

      def from_file(io)
        Enumerator.new do |yielder|
          agregator = ''
          enum = io.each_line

          loop do
            line = enum.next
            if line.start_with?('D, ', 'I, ', 'E, ')
              yielder << agregator unless agregator.empty?
              agregator = line
            else
              agregator << line unless agregator.empty?
            end
          end

          yielder << agregator
        end
      end
    end
  end
end
