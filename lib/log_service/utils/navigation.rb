module LogService
  module Utils
    module FileNavigation
      TIME_REGEXP = /\d{4}\-\d{2}\-\d{2}T\d{2}\:\d{2}\:\d{2}\.\d{6}/

      def goto_start_line(io)
        # Checking for end of file
        io.pos -= 1 if io.eof?
        # Checking for escape sequence at the end of line
        io.pos -= 1 if get_char(io) == "\n" && io.pos != 0
        # Looking for start of line
        io.pos -= 1 until get_char(io) == "\n" || io.pos.zero?
        # Shift pointer if we found escape sequence or the beginning of the file
        io.pos += 1 if get_char(io) == "\n"
        io.pos
      end

      def goto_prev_line(io)
        # Go to start of the current line
        goto_start_line(io)
        # Shift pointer to the previous line
        io.pos -= 1 unless io.pos.zero?
        # Go to start of the previouns line
        goto_start_line(io)
      end

      def goto_start_message(io)
        # Go to start of th currrent message
        goto_start_line(io)
        line = get_line(io)

        # Shift to the previous line until
        # we'll not found start of message or begin of file
        until line.start_with?('D, ', 'I, ', 'E, ') || io.pos.zero?
          goto_prev_line(io)
          line = get_line(io)
        end

        io.pos
      end

      def get_char(io)
        result = io.getc
        io.pos -= result.bytesize
        result if result.valid_encoding?
      end

      def get_line(io)
        start_pos = io.pos
        result = io.gets
        io.pos = start_pos
        result
      end
    end
  end
end
