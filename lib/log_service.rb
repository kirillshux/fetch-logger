# frozen_string_literal: true

require 'log_service/utils/message_enumerator'
require 'log_service/utils/navigation'
require 'log_service/file'
require 'log_service/strategy'

module LogService
  class FileIsNotValidError < StandardError; end
  class StrategyIsNotValidError < StandardError; end

  VERSION = '1.0.0'
end
