require 'sinatra'
require 'sinatra/activerecord'

set :database, "sqlite3:db/floggerdb.sqlite3"
set :server, :puma 

class MyApp < Sinatra::Base
  @title = "Log view"
  get '/' do
    haml :index
  end
end